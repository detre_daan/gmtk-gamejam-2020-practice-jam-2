﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Yeethouse : MonoBehaviour
{
    #region Variables
    [SerializeField] float power = 10f;
    [SerializeField] float raduius = 5f;
    [SerializeField] float upForce = 1.0f;
    #endregion

    public void DetonateMe(GameObject me)
    {
        Vector3 explosionPosition = transform.position;
        
        Rigidbody rb = me.GetComponent<Rigidbody>();

        rb.AddExplosionForce(power, explosionPosition, raduius, upForce, ForceMode.Impulse);
    }
}
