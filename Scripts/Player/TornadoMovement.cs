﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoMovement : MonoBehaviour {

    public float Damage = 2;

    [SerializeField] float acceleration = 5;
    [SerializeField] float maxVelocity = 40;

    Rigidbody rb;
    Vector2 currentVelocityHeading;
    Vector2 inputHeading;

    private void Start() {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate() {
        HandleInput();
        CalculateVelocity();
        Move();

    }

    private void HandleInput() {
       inputHeading = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    private void CalculateVelocity() {
        currentVelocityHeading += inputHeading.normalized * acceleration * Time.fixedDeltaTime;
        if (currentVelocityHeading.magnitude > maxVelocity) {
            currentVelocityHeading.Normalize();
            currentVelocityHeading *= maxVelocity;
        } 
    }
    
    private void Move() {
        Vector3 temp = new Vector3(currentVelocityHeading.x, currentVelocityHeading.y, 0f);
        transform.Translate(temp);
    }
}
