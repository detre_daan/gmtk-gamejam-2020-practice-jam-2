﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    #region Variables
    [SerializeField] GameObject goodBuild = null;
    [SerializeField] GameObject roof = null;
    [SerializeField] GameObject bottom = null;
    [SerializeField] GameObject animationHolder = null;

    [SerializeField] float healthMax = 10f;
    [SerializeField] float healthMin = 5f;
    [SerializeField] float damageDelayMain = 0.5f;
    [SerializeField] float flyTimeMax = 1.5f;
    [SerializeField] float flyTimeMin = 3f;
    [SerializeField] float mainHouse;

    Animator anim;
    HouseDialog dialogHandler;
    GameManager gameManager;

    bool flying = false;
    bool landed = false;
    bool gaveScore = false;

    float houseStartHealth = 0;
    float damageDelay;
    float flyTime = 0;
    #endregion

    private void Start()
    {
        SetupVariables();
    }

    private void SetupVariables()
    {
        anim = animationHolder.GetComponent<Animator>();
        dialogHandler = GetComponent<HouseDialog>();
        gameManager = FindObjectOfType<GameManager>().GetComponent<GameManager>();

        mainHouse = (int)Random.Range(healthMax, healthMin);
        flyTime = Random.Range(flyTimeMax, flyTimeMin);
        
        damageDelay = damageDelayMain;
        houseStartHealth = mainHouse;   
    }

    private void Update()
    {
        CheckForFlyHalt();
    }

    private void CheckForFlyHalt()
    {
        if (flying)
        {
            flyTime -= Time.deltaTime;
            if (flyTime <= 0)
            {
                roof.GetComponent<Rigidbody>().isKinematic = true;
                landed = true;
            }
        }
    }

    private void OnTriggerStay(Collider collider)
    {
        CheckForHouseDestruction(collider);
    }

    private void CheckForHouseDestruction(Collider collider)
    {
        damageDelay -= Time.deltaTime;

        if (collider.GetComponent<TornadoMovement>() && damageDelay <= 0 && !landed)
        {
            mainHouse -= collider.GetComponent<TornadoMovement>().Damage * Vector2.Distance(transform.position, collider.transform.position);

            if (mainHouse <= 0)
            {
                DestroyHouse(collider);

            }
            damageDelay = damageDelayMain;
        }
    }

    private void DestroyHouse(Collider collider)
    {
        roof.SetActive(true);
        collider.GetComponent<Yeethouse>().DetonateMe(roof);
        flying = true;

        if (!gaveScore)
        {
            FindObjectOfType<Audio>().GetComponent<Audio>().PlayBreak();
            GetComponent<Collider>().enabled = false;

            gameManager.score += houseStartHealth;
            gameManager.housesDestroyed += +1;            
            
            dialogHandler.DisplayDialog();

            anim.SetTrigger("destroy");

            bottom.SetActive(true);
            goodBuild.SetActive(false);
            gaveScore = true;
            collider.GetComponent<Yeethouse>().DetonateMe(roof);

        }
    }
}
