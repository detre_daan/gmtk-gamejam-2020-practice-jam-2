﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwable : MonoBehaviour
{
    [SerializeField] GameObject goodBuild = null;

    [SerializeField] float flyTimeMax = 1.5f;
    [SerializeField] float flyTimeMin = 3f;

    float flyTime = 0;
    bool flying = false;
    bool landed = false;

    private void Start()
    {
        flyTime = Random.Range(flyTimeMax, flyTimeMin);
    }

    private void Update()
    {
        if (flying)
        {
            flyTime -= Time.deltaTime;
            if (flyTime <= 0)
            {
                landed = true;
            }
        }
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.GetComponent<TornadoMovement>() /*&& !landed*/)
        {
            collider.GetComponent<Yeethouse>().DetonateMe(gameObject);
            flying = true;
        }
    }
}
