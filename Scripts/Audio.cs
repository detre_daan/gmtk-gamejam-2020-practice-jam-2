﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    [SerializeField] AudioClip music = null;
    [SerializeField] AudioClip[] breakSFX = null;

    public void PlayBreak()
    {
        int index = Random.Range(0, breakSFX.Length);

        AudioSource.PlayClipAtPoint(breakSFX[index], Camera.main.transform.position) ;
    }

}
