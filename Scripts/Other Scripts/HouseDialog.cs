﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseDialog : MonoBehaviour
{
    bool hasDisplayedDialog = false;
    public string dialog = "";
    GameManager manager = null;
    private void Start() {
        if (manager == null){
            manager = FindObjectOfType<GameManager>().GetComponent<GameManager>();
        }
    }
    public void DisplayDialog() {
        manager.HandleDialog(dialog);
    }
}
