﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    #region Variables
    public float time = 90f;
    public float housesDestroyed = 0;
    public float score = 0;

    [SerializeField] Queue<string> dialogQueue;

    [SerializeField] GameObject dialogBox;

    [Header("GameCanvas")]
    [SerializeField] GameObject gameCanvas = null;
    [SerializeField] TextMeshProUGUI dialogBoxText;
    [SerializeField] TextMeshProUGUI timeText = null;
    [SerializeField] TextMeshProUGUI housesDestroyedText = null;
    [SerializeField] TextMeshProUGUI scoreText = null;

    [Header ("EndGameCanvas")]
    [SerializeField] GameObject endGameCanvas = null;
    [SerializeField] TextMeshProUGUI endHousesDestroyedText = null;
    [SerializeField] TextMeshProUGUI endScoreText = null;
    [SerializeField] TextMeshProUGUI endText = null;
    [SerializeField] string noDeath = "Major Droei is happy that everything survived!";
    [SerializeField] string someDeath = "Major Droei is sad about Hitt and Bulk dying but luckily Shanti is alive!";
    [SerializeField] string allDeath = "Major Droei is to death to say how he feels!";

    [SerializeField] int timeBetweenDialogs = 10;
    [SerializeField] int dialogDuration = 10;

    int countdownNextDialog = 0;
    int countdownDialogFade = 0;
    #endregion

    private void Start() {
        if (gameCanvas != null)
        {
            countdownNextDialog = 0;
            dialogQueue = new Queue<string>();
        }
    }

    private void Update()
    {
        if (gameCanvas != null)
        {
            UpdateScores();
            UseDialog();
            EndGame();
        }

    }

    void EndGame()
    {
        int houseAmount = GameObject.Find("Houses").transform.childCount;
        if (time <= 0)
        {
            if (housesDestroyed == 0)
            {
                    endText.text = noDeath.ToString();

            }
            else if (houseAmount == housesDestroyed)
            {
                endText.text = allDeath.ToString();

            }
            else
            {
                endText.text = someDeath.ToString();
            }
            gameCanvas.SetActive(false);
            endGameCanvas.SetActive(true);
            endScoreText.text = score.ToString();
            endHousesDestroyedText.text = housesDestroyed.ToString();
            Time.timeScale = 0;
        }
    }

    private void UseDialog()
    {
        if (countdownDialogFade == 0)
        {
            FadeDialog();
            countdownDialogFade = dialogDuration;
        }
        else
        {
            countdownDialogFade--;
        }
        PushNextDialog();

        countdownNextDialog--;
    }

    private void UpdateScores()
    {
        time -= Time.deltaTime;
        timeText.text = Mathf.Round(time).ToString();
        housesDestroyedText.text = housesDestroyed.ToString();
        scoreText.text = score.ToString();
    }

    public void NextScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Quit() {
        Application.Quit();
    }

    public void HandleDialog(string dialog) {
        dialogQueue.Enqueue(dialog);
        PushNextDialog();
    }

    private void PushNextDialog() {
        if (dialogQueue.Count != 0 && countdownNextDialog <= 0) {
            if (!dialogBox.activeInHierarchy) {
                dialogBox.SetActive(true);
            }
            dialogBoxText.text = dialogQueue.Dequeue();
            countdownNextDialog = timeBetweenDialogs;
        }
    }

    private void FadeDialog() {
        dialogBox.SetActive(false);
    }

    public void Reload()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }
}
